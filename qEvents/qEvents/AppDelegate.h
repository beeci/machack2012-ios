//
//  AppDelegate.h
//  qEvents
//
//  Created by Bela Tibor Bartha on 12-05-04.
//  Copyright (c) 2012 Macadamian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CitySelectionViewController.h"
#import "WundergroundManager.h"

@class ScheduleViewController;
@class StorageManager;
@class EventfulManager;
@interface AppDelegate : UIResponder <UIApplicationDelegate, CitySelectionDelegate, WundergroundManagerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navigationController;
@property (strong, nonatomic) StorageManager *storageManager;
@property (strong, nonatomic) EventfulManager *eventfulManager;
@property (strong, readonly, nonatomic) NSArray *tenDayForecast;
@property (strong, nonatomic) NSString * userId;
@property (strong, nonatomic) NSString * city;
@property (strong, nonatomic) NSString * selectedVenueId;

- (void)switchScheduleControllerFrom:(ScheduleViewController *)oldController to:(ScheduleViewController *)newController withTransition:(UIViewAnimationTransition)transition;
- (void)showCitySelector;
@end
