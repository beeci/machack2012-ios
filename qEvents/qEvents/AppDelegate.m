//
//  AppDelegate.m
//  qEvents
//
//  Created by Bela Tibor Bartha on 12-05-04.
//  Copyright (c) 2012 Macadamian. All rights reserved.
//

#import "AppDelegate.h"

#import "ScheduleViewController.h"
#import "StorageManager.h"
#import "EventfulManager.h"
#import "UIConstants.h"
#import "UserInfo.h"
#import "CitySelectionViewController.h"
#import "WundergroundManager.h"
#import "SpinnerViewController.h"

@interface AppDelegate () {
    WundergroundManager *wunderGroundManager;
    ScheduleViewController *currentScheduleViewController;
}
@end

#include "CitySelectionViewController.h"

@implementation AppDelegate
@synthesize window = _window;
@synthesize navigationController = _navigationController;
@synthesize storageManager = _storageManager;
@synthesize eventfulManager = _eventfulManager;
@synthesize tenDayForecast = _tenDayForecast;
@synthesize userId = _userId;
@synthesize city = _city;
@synthesize selectedVenueId = _selectedVenueId;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.

    wunderGroundManager = [[WundergroundManager alloc] init];
    wunderGroundManager.delegate = self;
    
    [self showCitySelector];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(userInfoListArrived:)
                                                 name:kUserInfoListArrived
                                               object:nil];

    self.storageManager = [[StorageManager alloc] init];
    [self.storageManager initializeStorage];
    self.eventfulManager = [[EventfulManager alloc] init];
    
    if ([self.storageManager storageAvailable]) {
        [self.storageManager loadUserInfoList];
    }

    //this method was used only once, to initialize the storage with default user
    //[self.storageManager addUser:@"MACHACK2012" password:@"MACHACK2012"];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)switchScheduleControllerFrom:(ScheduleViewController *)oldController to:(ScheduleViewController *)newController withTransition:(UIViewAnimationTransition)transition {

    if (newController.view.superview == nil)
    {
        UIView *superView = oldController.view.superview;
        
        [UIView beginAnimations:@"View Flip" context:nil];
        [UIView setAnimationDuration:0.25];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        
        [UIView setAnimationTransition: transition
                               forView:superView cache:YES];
        [oldController.view removeFromSuperview];
        [superView insertSubview:newController.view atIndex:0];
    }
    [UIView commitAnimations];
    [self.navigationController setViewControllers:[NSArray arrayWithObject:newController] animated:NO];
    currentScheduleViewController = newController;
}

- (void)showCitySelector {
    CitySelectionViewController *citySelectionController = [[CitySelectionViewController alloc] initWithNibName:@"CitySelectionViewController" bundle:nil];
    citySelectionController.delegate = self;
    
    self.window.rootViewController = citySelectionController;
    [self.window makeKeyAndVisible];
}

#pragma mark citySelectionDelegate

- (void)selectedCity:(NSString *)city {
    
    self.city = city;
    [wunderGroundManager searchOnCity:city];
    SpinnerViewController *spinner = [[SpinnerViewController alloc] initWithNibName:@"SpinnerViewController" bundle:nil];
    self.window.rootViewController = spinner;
    [self.window makeKeyAndVisible];
}

#pragma mark wundergroundDelegate
- (void)resultArrived {
    _tenDayForecast = wunderGroundManager.tenDayForecast;
    
    currentScheduleViewController = [[ScheduleViewController alloc] initWithNibName:@"ScheduleViewController" bundle:nil date:[NSDate date]];
    self.navigationController = [[UINavigationController alloc] initWithRootViewController:currentScheduleViewController];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.window.rootViewController = self.navigationController;
    [self.window makeKeyAndVisible];
}

- (NSString *) authenticateUser : (NSString *) userName password : (NSString *) password {
 
    NSString * userGUID = @"";
    
    for (UserInfo * userInfo in self.storageManager.userInfoList) {
        if ([userInfo.userName isEqualToString:userName] && [userInfo.password isEqualToString:password]) {
            userGUID = userInfo.userId;
            break;
        }
    }
    
    return userGUID;
}

- (void)userInfoListArrived:(NSNotification *)notification {
 
    self.userId = [self authenticateUser:@"MACHACK2012" password:@"MACHACK2012"];
    
    if (![self.userId isEqualToString:@""]) 
        NSLog(@"User successfully authenticated");
    
    // user list is ready we can load events
    [self.storageManager loadEventInfoList];
}

@end
