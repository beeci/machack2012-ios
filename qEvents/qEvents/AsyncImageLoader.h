//
//  AsyncImage.h
//  qEvents
//
//  Created by Bela Tibor Bartha on 12-05-04.
//  Copyright (c) 2012 Macadamian. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AsyncImageLoader : NSObject {
	//could instead be a subclass of UIImageView instead of UIView, depending on what other features you want to 
	// to build into this class?

	NSURLConnection* connection;
    NSMutableData* data;
}

- (void)loadImageFromURL:(NSURL*)url;
@property (weak, nonatomic) UIImageView *loadToImageView;

@end
