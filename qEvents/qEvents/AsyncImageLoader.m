//
//  AsyncImage.m
//  qEvents
//
//  Created by Bela Tibor Bartha on 12-05-04.
//  Copyright (c) 2012 Macadamian. All rights reserved.
//
#import "AsyncImageLoader.h"

@implementation AsyncImageLoader
@synthesize loadToImageView;

- (void)loadImageFromURL:(NSURL*)url {
	connection = nil;
	data = nil;
	
	NSURLRequest* request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
	connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void)connection:(NSURLConnection *)theConnection didReceiveData:(NSData *)incrementalData {
	if (data == nil) {
        data = [[NSMutableData alloc] initWithCapacity:2048];
    } 
	[data appendData:incrementalData];
}

- (void)connectionDidFinishLoading:(NSURLConnection*)theConnection {
    
	connection = nil;
    loadToImageView.image = [UIImage imageWithData:data]; 
	data = nil;
}

@end
