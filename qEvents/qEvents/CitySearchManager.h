//
//  CitySearchManager.h
//  qEvents
//
//  Created by Maria on 5/5/12.
//  Copyright (c) 2012 Macadamian. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SBJsonStreamParser;
@class SBJsonStreamParserAdapter;

@protocol CitySearchManagerDelegate <NSObject>

- (void)resultArrived;

@end

@interface CitySearchManager : NSObject {
    SBJsonStreamParser *parser;
    NSURLConnection *theConnection;
    SBJsonStreamParserAdapter *adapter;
}

- (void) searchOnCity : (NSString *)city;

@property (strong, readonly, nonatomic) NSArray *cities;
@property (weak, nonatomic) id<CitySearchManagerDelegate> delegate;

@end
