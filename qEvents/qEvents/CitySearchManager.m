//
//  CitySearchManager.m
//  qEvents
//
//  Created by Maria on 5/5/12.
//  Copyright (c) 2012 Macadamian. All rights reserved.
//

#import "CitySearchManager.h"
#import "SBJson.h"
#import "UIConstants.h"

static NSString *citySearchURL = @"http://autocomplete.wunderground.com/aq?query=%@&format=JSON";

@interface CitySearchManager () <SBJsonStreamParserAdapterDelegate>

@end;

@implementation CitySearchManager
@synthesize cities = _cities;
@synthesize delegate;

- (void) searchOnCity : (NSString *)city {
    // We don't want *all* the individual messages from the
	// SBJsonStreamParser, just the top-level objects. The stream
	// parser adapter exists for this purpose.
	adapter = [[SBJsonStreamParserAdapter alloc] init];
	
	// Set ourselves as the delegate, so we receive the messages
	// from the adapter.
	adapter.delegate = self;
	
	// Create a new stream parser..
	parser = [[SBJsonStreamParser alloc] init];
	
	// .. and set our adapter as its delegate.
	parser.delegate = adapter;
	
	// Normally it's an error if JSON is followed by anything but
	// whitespace. Setting this means that the parser will be
	// expecting the stream to contain multiple whitespace-separated
	// JSON documents.
	parser.supportMultipleDocuments = YES;
    
    NSString *url = [NSString stringWithFormat:citySearchURL, city];
    
	NSURLRequest *theRequest=[NSURLRequest requestWithURL:[NSURL URLWithString:url]
											  cachePolicy:NSURLRequestUseProtocolCachePolicy
										  timeoutInterval:60.0];
	
	theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
}

#pragma mark SBJsonStreamParserAdapterDelegate methods

- (void)parser:(SBJsonStreamParser *)parser foundArray:(NSArray *)array {
    [NSException raise:@"unexpected" format:@"Should not get here"];
}

- (void)parser:(SBJsonStreamParser *)parser foundObject:(NSDictionary *)dict {
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:[NSString stringWithFormat:@"%@", kSearchResultArrived]
                                                  object:dict];
    NSDictionary *citiesDict = [dict objectForKey:@"RESULTS"];
    
    NSMutableArray *list = [NSMutableArray arrayWithCapacity:citiesDict.count];
    for (NSDictionary *cityDict in citiesDict) {
        [list addObject:[cityDict objectForKey:@"name"]];
    }
    _cities = [NSArray arrayWithArray:list];
    [delegate resultArrived];
}

#pragma mark NSURLConnectionDelegate methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	NSLog(@"Connection didReceiveResponse: %@ - %@", response, [response MIMEType]);
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
	NSLog(@"Connection didReceiveAuthenticationChallenge: %@", challenge);
	
	/*NSURLCredential *credential = [NSURLCredential credentialWithUser:username.text
     password:password.text
     persistence:NSURLCredentialPersistenceForSession];
     
     [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];*/
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	NSLog(@"Connection didReceiveData of length: %u", data.length);
    
    NSString * result = [[NSString alloc]  initWithBytes:[data bytes] length:[data length] encoding:NSUTF8StringEncoding];
    
    NSLog(@"%@", result);
	
	// Parse the new chunk of data. The parser will append it to
	// its internal buffer, then parse from where it left off in
	// the last chunk.
	SBJsonStreamParserStatus status = [parser parse:data];
	
	if (status == SBJsonStreamParserError) {
		NSLog(@"Parser error: %@", parser.error);
		
	} else if (status == SBJsonStreamParserWaitingForData) {
		NSLog(@"Parser waiting for more data");
	}
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"Connection failed! Error - %@ %@",
          [error localizedDescription],
          [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
}

@end
