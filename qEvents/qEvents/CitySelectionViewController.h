//
//  CitySelectionViewController.h
//  qEvents
//
//  Created by Maria on 5/5/12.
//  Copyright (c) 2012 Macadamian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CitySearchManager.h"

@protocol CitySelectionDelegate <NSObject>

- (void)selectedCity:(NSString *)city;

@end

@interface CitySelectionViewController : UITableViewController <UISearchBarDelegate, CitySearchManagerDelegate, UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) id<CitySelectionDelegate> delegate;

@end
