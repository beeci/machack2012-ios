//
//  DetailsViewController.h
//  qEvents
//
//  Created by Maria on 5/4/12.
//  Copyright (c) 2012 Macadamian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailsViewController : UIViewController

@property (nonatomic) BOOL tweetButtonEnabled;
@property (strong, nonatomic) NSString *eventName;
@property (strong, nonatomic) NSString *eventDetails;
@property (strong, nonatomic) NSString *eventID;
@property (strong, nonatomic) NSString *startDate;
@property (strong, nonatomic) NSString *endDate;

@end
