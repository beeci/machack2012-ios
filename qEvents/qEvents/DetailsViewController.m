//
//  DetailsViewController.m
//  qEvents
//
//  Created by Maria on 5/4/12.
//  Copyright (c) 2012 Macadamian. All rights reserved.
//

#import "DetailsViewController.h"
#import <Twitter/Twitter.h>
#import "CitySelectionViewController.h"

#define letOSHandleLogin FALSE

@interface DetailsViewController ()

- (IBAction)tweetButtonPressed:(id)sender;
- (IBAction)selectCityButtonPressed:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *tweetButton;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UITextView *detailTextView;
@property (weak, nonatomic) IBOutlet UILabel *startDateLabel;
@end

@implementation DetailsViewController
@synthesize tweetButton = tweetButton_;
@synthesize eventName = _eventName;
@synthesize eventDetails = _eventDetails;
@synthesize eventID = _eventID;
@synthesize nameLabel = _nameLabel;
@synthesize detailTextView = _detailTextView;
@synthesize startDateLabel = _startDateLabel;
@synthesize startDate = _startDate;
@synthesize endDate = _endDate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _nameLabel.text = _eventName;
    _detailTextView.text = _eventDetails;
    _startDateLabel.text = _startDate;
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)tweetButtonPressed:(id)sender {
    //Create the tweet sheet
    TWTweetComposeViewController *tweetSheet = [[TWTweetComposeViewController alloc] init];
    
    [tweetSheet setInitialText:@"Check this event: "];
    [tweetSheet addURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://eventful.com/events/%@", self.eventID]]];
    
    //Set a blocking handler for the tweet sheet
    tweetSheet.completionHandler = ^(TWTweetComposeViewControllerResult result){
        [self dismissModalViewControllerAnimated:YES];
    };
    
    //Show the tweet sheet!
    [self presentModalViewController:tweetSheet animated:YES];
}

-(BOOL) tweetButtonEnabled {
    return self.tweetButton.enabled;
}

- (void) setTweetButtonEnabled:(BOOL)tweetButtonEnabled {
    self.tweetButton.enabled = tweetButtonEnabled;
}

- (IBAction)selectCityButtonPressed:(id)sender {
    CitySelectionViewController *citySelectionViewController = [[CitySelectionViewController alloc]initWithNibName:@"CitySelectionViewController" bundle:[NSBundle mainBundle]];
    [[self navigationController] pushViewController:citySelectionViewController animated:YES];
}
@end
