//
//  EditDetailsCell.h
//  qEvents
//
//  Created by Bela Tibor Bartha on 12-05-05.
//  Copyright (c) 2012 Macadamian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditDetailsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextView *textView;

@end
