//
//  EditDetailsCell.m
//  qEvents
//
//  Created by Bela Tibor Bartha on 12-05-05.
//  Copyright (c) 2012 Macadamian. All rights reserved.
//

#import "EditDetailsCell.h"

@implementation EditDetailsCell
@synthesize textView = _textView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
