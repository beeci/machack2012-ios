//
//  EventData.h
//  qEvents
//
//  Created by Zoli on 5/5/12.
//  Copyright (c) 2012 Macadamian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EventData : NSObject

@property (strong, nonatomic) NSString * title;
@property (strong, nonatomic) NSString * description;
@property (strong, nonatomic) NSDate * startDate;
@property (strong, nonatomic) NSDate * endDate;
@property (strong, nonatomic) NSString * venueName;
@property (strong, nonatomic) NSString * venueAddress;
@property (strong, nonatomic) NSString * eventId;

+ (NSArray *) parseJSON : (NSDictionary *) jsonData;

@end
