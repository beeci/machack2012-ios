//
//  EventData.m
//  qEvents
//
//  Created by Zoli on 5/5/12.
//  Copyright (c) 2012 Macadamian. All rights reserved.
//

#import "EventData.h"
#import "NSDateFormatter+Helper.h"

@implementation EventData

@synthesize title;
@synthesize description;
@synthesize startDate;
@synthesize endDate;
@synthesize venueName;
@synthesize venueAddress;
@synthesize eventId;

+ (NSArray *) parseJSON : (NSDictionary *) jsonData {
    
    NSMutableArray * array = [NSMutableArray array];
    
    NSDictionary * events = [jsonData objectForKey:@"events"];
    
    if ([events isKindOfClass:[NSNull class]]) {
        return nil;
    }
    
    NSArray * eventList = [events objectForKey:@"event"];
    

    
    for (NSDictionary * event in eventList) {
        
        if (![event respondsToSelector:@selector(keyEnumerator)]) {
            NSLog(@"Something wrong with the received data");
            continue;
        }
        
        EventData * eventData = [[EventData alloc] init];
        eventData.eventId = [event objectForKey:@"id"];
        eventData.title = [event objectForKey:@"title"];

        NSObject *description = [event objectForKey:@"description"];
        
        if ([description isKindOfClass:[NSString class]])
            eventData.description = [event objectForKey:@"description"];
        
        NSString * startTimeString = [event objectForKey:@"start_time"];
        NSString * stopTimeString = [event objectForKey:@"stop_time"];
        
        if (![startTimeString isKindOfClass:[NSNull class]])
            eventData.startDate = [[NSDateFormatter eventfulReceivedDateFormatter] dateFromString:startTimeString];
        if (![stopTimeString isKindOfClass:[NSNull class]])
            eventData.endDate = [[NSDateFormatter eventfulReceivedDateFormatter] dateFromString:stopTimeString];
        
        eventData.venueAddress = [event objectForKey:@"venue_address"];
        eventData.venueName = [event objectForKey:@"venue_name"];

        [array addObject:eventData];
    }
    
    return array;
}

@end
