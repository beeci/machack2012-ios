//
//  EventDetailEditViewController.h
//  qEvents
//
//  Created by Bela Tibor Bartha on 12-05-05.
//  Copyright (c) 2012 Macadamian. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol EventDetailEditProtocol <NSObject>

- (void)setText:(NSString *)text isPlace: (BOOL) isPlace;

@end

@interface EventDetailEditViewController : UIViewController <UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) id<EventDetailEditProtocol> delegate;

@end
