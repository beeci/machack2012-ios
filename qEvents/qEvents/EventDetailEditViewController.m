//
//  EventDetailEditViewController.m
//  qEvents
//
//  Created by Bela Tibor Bartha on 12-05-05.
//  Copyright (c) 2012 Macadamian. All rights reserved.
//

#import "EventDetailEditViewController.h"

@interface EventDetailEditViewController ()

@end

@implementation EventDetailEditViewController
@synthesize textView = _textView;
@synthesize delegate = _delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] 
                                   initWithTitle:@"Done"
                                   style:UIBarButtonItemStyleBordered
                                   target:self
                                   action:@selector(donePressed:)];
    self.navigationItem.rightBarButtonItem = doneButton;
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark TextViewDelegate
- (void)textViewDidEndEditing:(UITextView *)textView {
    
    [_delegate setText:textView.text isPlace:NO];
}

#pragma mark button handling
- (IBAction)donePressed:(id)sender {
    [_delegate setText:self.textView.text isPlace:NO];
}


@end
