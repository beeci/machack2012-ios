//
//  EventInfo.h
//  qEvents
//
//  Created by Zoli on 5/5/12.
//  Copyright (c) 2012 Macadamian. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    userIdKey,
    startDateKey,
    eventIdKey,
} fieldsTypeEventInfo;

@interface EventInfo : UIDocument

@property (strong, nonatomic) NSString * userId;
@property (strong, nonatomic) NSDate * startDate;
@property (strong, nonatomic) NSString * eventId;

@end
