//
//  EventInfo.m
//  qEvents
//
//  Created by Zoli on 5/5/12.
//  Copyright (c) 2012 Macadamian. All rights reserved.
//

#import "EventInfo.h"
#import "NSDateFormatter+Helper.h"
#import "NSDate+Helper.h"

@implementation EventInfo

@synthesize userId;
@synthesize startDate;
@synthesize eventId;

- (NSData *) serialize {
    
    NSString * serializedData = [NSString stringWithFormat:@"%@|%@|%@", self.userId, [[NSDateFormatter cloudEventDateFormatter] stringFromDate:self.startDate], self.eventId];
    
    return [NSData dataWithBytes:[serializedData UTF8String] length:[serializedData length]];;
}

- (void) deserialize : (NSData *) data {
    
    NSString * stringData = [[NSString alloc]  initWithBytes:[data bytes] length:[data length] encoding:NSUTF8StringEncoding];
    
    NSArray * fields = [stringData componentsSeparatedByString:@"|"];
    
    for (int i = 0; i < fields.count; i++) {
        
        NSString * field = [fields objectAtIndex:i];
        
        switch (i) {
            case userIdKey:
                self.userId = field;
                break;
            case startDateKey:
                self.startDate = [NSDate dateFromCloudString:field];
                break;
            case eventIdKey:
                self.eventId = field;
                break;
            default:
                break;
        }
    }
    
}

// Called whenever the application reads data from the file system
- (BOOL)loadFromContents:(id)contents ofType:(NSString *)typeName error:(NSError **)outError{
    
    if ([contents length] > 0) {    
        [self deserialize:contents];
    }
    
    return YES;    
}

// Called whenever the application (auto)saves the content of a note
- (id)contentsForType:(NSString *)typeName error:(NSError **)outError 
{
    return [self serialize];
    
}


@end
