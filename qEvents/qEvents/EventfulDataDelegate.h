//
//  EventfulDataDelegate.h
//  qEvents
//
//  Created by Zoli on 5/5/12.
//  Copyright (c) 2012 Macadamian. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    SearchResult,
    NewEventResult,
    VenueListResult,
} EventfulDataType;

@protocol EventfulDataDelegate <NSObject>
- (void) eventfulDataReceived : (NSDictionary * ) data type : (EventfulDataType) type;
@end
