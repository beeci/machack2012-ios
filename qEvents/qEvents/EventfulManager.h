//
//  EventfulManager.h
//  qEvents
//
//  Created by Zoli on 5/4/12.
//  Copyright (c) 2012 Macadamian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EventfulDataDelegate.h"

@class SBJsonStreamParser;
@class SBJsonStreamParserAdapter;

@interface EventfulManager : NSObject {
    SBJsonStreamParser *parser;
    NSURLConnection *theConnection;
    SBJsonStreamParserAdapter *adapter;
    EventfulDataType returnableData;
}

@property (strong, nonatomic) id <EventfulDataDelegate> delegate;

- (void) searchOnCity : (NSString *) city startDate : (NSDate *) startDate endDate : (NSDate *) endDate;
- (void) searchOnCity:(NSString *)city day : (NSDate *) day;
- (void) newEvent : (NSString *) title description: (NSString *) description venueId: (NSString *) venueId startDate : (NSDate *) startDate; 
- (void)searchVenueList: (NSString * ) city;

@end
