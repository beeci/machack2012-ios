//
//  EventfulManager.m
//  qEvents
//
//  Created by Zoli on 5/4/12.
//  Copyright (c) 2012 Macadamian. All rights reserved.
//

#import "EventfulManager.h"
#import "SBJson.h"
#import "UIConstants.h"
#import "NSDateFormatter+Helper.h"

// instead specifying city name we need to create a venue on server, for time considerations, we are hardcoding this venue right now
static NSString * clujVenueId = @"V0-001-006102198-8";
static NSString * app_key = @"P9Z4Zcrw8MZ4W87h"; // application key set on eventful
static NSString * searchURL = @"http://api.evdb.com/json/events/search?app_key=%@&location=%@&page_size=100&date=%@";
//http://api.evdb.com/rest/events/new?app_key=P9Z4Zcrw8MZ4W87h&user=MACHACK2012&password=MACHACK2012&title=TestEvent&venue_id=V0-001-006102198-8&start_time=2012-05-06+17:00:00
static NSString * newEventURL = @"http://api.evdb.com/json/events/new?app_key=%@&user=MACHACK2012&password=MACHACK2012&title=%@&description=%@&venue_id=%@&start_time=%@";
static NSString * searchVenueList = @"http://api.eventful.com/json/venues/search?app_key=%@&page_size=20&location=%@";
@interface EventfulManager () <SBJsonStreamParserAdapterDelegate>

@end

@implementation EventfulManager
@synthesize delegate = _delegate;

- (NSString *) formatDateInterval : (NSDate *) startDate endDate : (NSDate *) endDate {
    
    NSString * result = [NSString stringWithFormat:@"%@-%@", [[NSDateFormatter eventfulDateFormatter] stringFromDate:startDate], [[NSDateFormatter eventfulDateFormatter] stringFromDate:endDate]];
    return result;
}

- (void) callServerWithURL : (NSString *) url {
    
    adapter = [[SBJsonStreamParserAdapter alloc] init];
	
	// Set ourselves as the delegate, so we receive the messages
	// from the adapter.
	adapter.delegate = self;
	
	// Create a new stream parser..
	parser = [[SBJsonStreamParser alloc] init];
	
	// .. and set our adapter as its delegate.
	parser.delegate = adapter;
    
	parser.supportMultipleDocuments = YES;
    
    NSLog(@"URL %@ called.", url);
    
    NSURLRequest *theRequest=[NSURLRequest requestWithURL:[NSURL URLWithString:url]
											  cachePolicy:NSURLRequestUseProtocolCachePolicy
										  timeoutInterval:30.0];
	
	theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];

}

- (void)searchVenueList: (NSString * ) city {
    NSString * url = [NSString stringWithFormat:searchVenueList, app_key, [city stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    returnableData = VenueListResult;
    
    [self callServerWithURL:url];
}

- (void) newEvent : (NSString *) title description: (NSString *) description venueId: (NSString *) venueId startDate : (NSDate *) startDate {
    NSString * date = [[NSDateFormatter eventfulNewEventDateFormatter] stringFromDate:startDate];
    returnableData = NewEventResult;
    NSString * url = [NSString stringWithFormat:newEventURL, app_key, [title stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding], [description stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding], venueId, date ];
    
    [self callServerWithURL:url];
}

- (void) searchOnCity:(NSString *)city day : (NSDate *) date {
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    returnableData = SearchResult;
    
    NSDateComponents *dateComponents = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:date];
    [dateComponents setHour:0];
    [dateComponents setMinute:0];
    [dateComponents setSecond:0];
    
    [dateComponents setDay:[dateComponents day] + 1];
    NSDate *startDate = [calendar dateFromComponents:dateComponents]; // yesterday midnight
    [dateComponents setDay:[dateComponents day] + 1];
    NSDate *endDate = [calendar dateFromComponents:dateComponents]; // today midnight

    [self searchOnCity:city startDate:startDate endDate:endDate];
}

- (void) searchOnCity : (NSString *) city startDate : (NSDate *) startDate endDate : (NSDate *) endDate {

	adapter = [[SBJsonStreamParserAdapter alloc] init];

    NSString * url = [NSString stringWithFormat:searchURL, app_key, [city stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding], [self formatDateInterval:startDate endDate:endDate]];
    
    [self callServerWithURL:url];
}

#pragma mark SBJsonStreamParserAdapterDelegate methods

- (void)parser:(SBJsonStreamParser *)parser foundArray:(NSArray *)array {
    [NSException raise:@"unexpected" format:@"Should not get here"];
}

- (void)parser:(SBJsonStreamParser *)parser foundObject:(NSDictionary *)dict {

    // we could have the case when a search is performed right after a new, and because we are using the same event handler
    // we cannot know which response arrives. 3 length message will be considered new event response
    
    if ([dict objectForKey:@"events"] != nil)
        returnableData = SearchResult;
    
    if ([dict objectForKey:@"venues"] != nil)
        returnableData = VenueListResult;
    
    if ([dict count] == 3)
        returnableData = NewEventResult;
    
    if (self.delegate)
        [self.delegate eventfulDataReceived:dict type:returnableData];
}

#pragma mark NSURLConnectionDelegate methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	NSLog(@"Connection didReceiveResponse: %@ - %@", response, [response MIMEType]);
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
	NSLog(@"Connection didReceiveAuthenticationChallenge: %@", challenge);
	
	/*NSURLCredential *credential = [NSURLCredential credentialWithUser:username.text
															 password:password.text
														  persistence:NSURLCredentialPersistenceForSession];
	
	[[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];*/
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	NSLog(@"Connection didReceiveData of length: %u", data.length);
	
	// Parse the new chunk of data. The parser will append it to
	// its internal buffer, then parse from where it left off in
	// the last chunk.
	SBJsonStreamParserStatus status = [parser parse:data];
	
	if (status == SBJsonStreamParserError) {
		NSLog(@"Parser error: %@", parser.error);
		
	} else if (status == SBJsonStreamParserWaitingForData) {
		NSLog(@"Parser waiting for more data");
	}
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"Connection failed! Error - %@ %@",
          [error localizedDescription],
          [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
}

@end
