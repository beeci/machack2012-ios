//
//  NSDate+Helper.h
//  qEvents
//
//  Created by Bela Tibor Bartha on 12-05-04.
//  Copyright (c) 2012 Macadamian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Helper)

- (NSDate *) clearedTime;
- (NSDate *) setHours:(NSInteger) hours;
- (int) hour;
- (int) minute;
+ (NSDate *) dateFromYear:(NSInteger)year month:(NSInteger)month day:(NSInteger)day;
+ (NSDate *) dateFromCloudString:(NSString *)dateString;


@end
