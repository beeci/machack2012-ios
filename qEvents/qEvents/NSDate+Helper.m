//
//  NSDate+Helper.m
//  qEvents
//
//  Created by Bela Tibor Bartha on 12-05-04.
//  Copyright (c) 2012 Macadamian. All rights reserved.
//

#import "NSDate+Helper.h"

@implementation NSDate (Helper)

- (NSDate *) clearedTime {
    
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comps = [calendar components:unitFlags fromDate:self];
    
    [comps setHour:0];
    [comps setMinute:0];
    [comps setSecond:0];
    
    return [calendar dateFromComponents:comps];
}

- (NSDate *) setHours:(NSInteger) hours {
    
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comps = [calendar components:unitFlags fromDate:self];
    
    [comps setHour:hours];
    [comps setMinute:0];
    [comps setSecond:0];
    
    return [calendar dateFromComponents:comps];
}

+ (NSDate *) dateFromCloudString:(NSString *)dateString {
    
    NSArray *components = [dateString componentsSeparatedByString:@"-"];
    
    if (components.count < 4) {
        return nil;
    }
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [calendar setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    
    [comps setYear:[(NSString *)[components objectAtIndex:0] intValue]];
    [comps setMonth:[(NSString *)[components objectAtIndex:1] intValue]];
    [comps setDay:[(NSString *)[components objectAtIndex:2] intValue]];
    [comps setHour:[(NSString *)[components objectAtIndex:3] intValue]];
    [comps setMinute:0];
    [comps setSecond:0];
    
    NSDate *result = [calendar dateFromComponents:comps];
    
    return result;
}

+ (NSDate *) dateFromYear:(NSInteger)year month:(NSInteger)month day:(NSInteger)day {
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    
    [comps setYear:year];
    [comps setMonth:month];
    [comps setDay:day];
    [comps setHour:0];
    [comps setMinute:0];
    [comps setSecond:0];
    
    return [calendar dateFromComponents:comps];
}

- (int) hour {
    unsigned unitFlags =  NSHourCalendarUnit | NSMinuteCalendarUnit;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    [calendar setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    NSDateComponents *comps = [calendar components:unitFlags fromDate:self];
    
    return comps.hour;
}

- (int) minute {
    unsigned unitFlags =  NSHourCalendarUnit | NSMinuteCalendarUnit;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
        [calendar setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    NSDateComponents *comps = [calendar components:unitFlags fromDate:self];
    
    return comps.minute;
}

@end
