//
//  NSDateFormatter+Helper.h
//  qEvents
//
//  Created by Bela Tibor Bartha on 12-05-04.
//  Copyright (c) 2012 Macadamian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDateFormatter (Helper)
+ (NSDateFormatter *)shortDateFormatter;
+ (NSDateFormatter *)eventfulDateFormatter;
+ (NSDateFormatter*)eventfulReceivedDateFormatter;
+ (NSDateFormatter*)eventfulNewEventDateFormatter;
+ (NSDateFormatter*)cloudEventDateFormatter;
@end
