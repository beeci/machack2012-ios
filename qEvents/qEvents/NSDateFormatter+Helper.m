//
//  NSDateFormatter+Helper.m
//  qEvents
//
//  Created by Bela Tibor Bartha on 12-05-04.
//  Copyright (c) 2012 Macadamian. All rights reserved.
//

#import "NSDateFormatter+Helper.h"

@implementation NSDateFormatter (Helper)

+ (NSDateFormatter *)shortDateFormatter {
    
    static NSDateFormatter *formatter;
    if (!formatter) {
        formatter = [[NSDateFormatter alloc] init]; 
        formatter.timeStyle = NSDateFormatterNoStyle;
        formatter.dateStyle = NSDateFormatterShortStyle;
    }
return formatter;
}

+ (NSDateFormatter *)eventfulDateFormatter {
    
    static NSDateFormatter *formatter;
    if (!formatter) {
        formatter = [[NSDateFormatter alloc] init]; 
        formatter.dateFormat = @"yyyyMMdd00";
        formatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    }
    return formatter;
}

+ (NSDateFormatter*)eventfulReceivedDateFormatter {
    static NSDateFormatter *formatter;
    if (!formatter) {
        formatter = [[NSDateFormatter alloc] init]; 
        formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
        formatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    }
    return formatter;
}

+ (NSDateFormatter*)eventfulNewEventDateFormatter {
    static NSDateFormatter *formatter;
    if (!formatter) {
        formatter = [[NSDateFormatter alloc] init]; 
        formatter.dateFormat = @"yyyy-MM-dd+HH:mm:ss";
    }
    return formatter;
}

+ (NSDateFormatter*)cloudEventDateFormatter {
    static NSDateFormatter *formatter;
    if (!formatter) {
        formatter = [[NSDateFormatter alloc] init]; 
        formatter.dateFormat = @"yyyy-MM-dd-HH";
    }
    return formatter;
}


@end
