//
//  NewEventViewController.h
//  qEvents
//
//  Created by Zoli on 5/5/12.
//  Copyright (c) 2012 Macadamian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventDetailEditViewController.h"

@interface NewEventViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, EventDetailEditProtocol>
@property (nonatomic, strong) NSDictionary * venuesDictionary;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil date:(NSDate *)eventDate;
@end
