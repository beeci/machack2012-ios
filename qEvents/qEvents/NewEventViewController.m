//
//  NewEventViewController.m
//  qEvents
//
//  Created by Zoli on 5/5/12.
//  Copyright (c) 2012 Macadamian. All rights reserved.
//

#import "NewEventViewController.h"
#import "EditNameCell.h"
#import "EditDetailsCell.h"
#import "AppDelegate.h"
#import "EventfulManager.h"
#import "StorageManager.h"
#import "EventDetailEditViewController.h"
#import "PlaceViewController.h"

@interface NewEventViewController () {
    NSDate *date;
}

@property (strong, nonatomic) IBOutlet EditNameCell *editNameCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *editDetailsCell;
@property (strong, nonatomic) IBOutlet EditNameCell *twitterHashCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *setPlaceCell;
@property (weak, nonatomic) IBOutlet UITableView *table;

@property (weak, nonatomic) IBOutlet UILabel *placeLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailsLabel;

@end

@implementation NewEventViewController
@synthesize editNameCell = _editNameCell;
@synthesize editDetailsCell = _editDetailsCell;
@synthesize twitterHashCell = _twitterHashCell;
@synthesize setPlaceCell = _setPlaceCell;
@synthesize table = _table;
@synthesize placeLabel = _placeLabel;
@synthesize detailsLabel = _detailsLabel;
@synthesize venuesDictionary = _venuesDictionary;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil date:(NSDate *)eventDate
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        date = eventDate;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] 
                                   initWithTitle:@"Done"
                                   style:UIBarButtonItemStyleBordered
                                   target:self
                                   action:@selector(donePressed:)];
    self.navigationItem.rightBarButtonItem = doneButton;
    self.title = @"New Event";
}

- (void)viewDidUnload
{
    [self setSetPlaceCell:nil];
    [self setPlaceLabel:nil];
    [self setDetailsLabel:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
            return self.editNameCell;
            break;
        case 1:
            return self.editDetailsCell;
            break;
        case 2:
            return self.setPlaceCell;
            break;
        case 3:
            return self.twitterHashCell;
            break;
        default:
            break;
    }    
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    switch (indexPath.row) {
        case 0:
            return  self.editNameCell.frame.size.height;
            break;
        case 1:
            return self.editDetailsCell.frame.size.height;
            break;
        case 2:
            return self.setPlaceCell.frame.size.height;
            break;
        default:
            return 0;
    }
    return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 1) {
        EventDetailEditViewController *newController = [[EventDetailEditViewController alloc] initWithNibName:@"EventDetailEditViewController" bundle:nil];
        if (newController.view) {
            newController.delegate = self;
            newController.textView.text = self.editDetailsCell.detailTextLabel.text;
        }
        [[self navigationController] pushViewController:newController animated:YES];

    }
    else if (indexPath.row == 2) {
        PlaceViewController *placeViewController = [[PlaceViewController alloc] initWithNibName:@"PlaceViewController" bundle:nil];
        [placeViewController setTitle:@"Select a Place"];
        [placeViewController setVenuesList:self.venuesDictionary];
        [placeViewController setDelegate:self];
        
        [[self navigationController] pushViewController:placeViewController animated:YES];
    }
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    
    EventDetailEditViewController *newController = [[EventDetailEditViewController alloc] initWithNibName:@"EventDetailEditViewController" bundle:nil];
    [newController setTitle:@"Event Details"];
    if (newController.view) {
        newController.delegate = self;
        newController.textView.text = self.detailsLabel.text;
    }
    [[self navigationController] pushViewController:newController animated:YES];
}

- (void)setText:(NSString *)text isPlace:(BOOL)isPlace {
    if (!isPlace)
        self.detailsLabel.text = text;
    else {
        self.placeLabel.text = text;
    }
    [self.table reloadData];
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark button handling
- (IBAction)donePressed:(id)sender {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDelegate.selectedVenueId.length == 0) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Place was not selected"
                                                            message:@"Unable to create event. Place was not choosed!"
                                                           delegate:self cancelButtonTitle: @"OK"
                                                  otherButtonTitles:nil];
        
        [alertView show];
    }else {
        [appDelegate.eventfulManager newEvent:self.editNameCell.textField.text description:self.detailsLabel.text venueId: appDelegate.selectedVenueId startDate:date];
    }

    [self.navigationController popViewControllerAnimated:YES];
}

@end
