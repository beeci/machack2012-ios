//
//  PlaceViewController.h
//  qEvents
//
//  Created by Maria on 5/5/12.
//  Copyright (c) 2012 Macadamian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventDetailEditViewController.h"

@interface PlaceViewController : UITableViewController

@property (nonatomic, strong) NSDictionary * venuesList;
@property (nonatomic) int selectedIndex;
@property (weak, nonatomic) id<EventDetailEditProtocol> delegate;

@end
