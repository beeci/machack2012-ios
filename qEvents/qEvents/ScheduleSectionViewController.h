//
//  ScheduleSectionViewController.h
//  qEvents
//
//  Created by Bela Tibor Bartha on 12-05-05.
//  Copyright (c) 2012 Macadamian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScheduleSectionViewController : UIViewController

@property (strong, nonatomic) UIImage *weatherIcon;

- (void)setTemperature:(NSInteger)temperature;
- (void)setImageURL:(NSString *)imageUrl;

@end
