//
//  ScheduleSectionViewController.m
//  qEvents
//
//  Created by Bela Tibor Bartha on 12-05-05.
//  Copyright (c) 2012 Macadamian. All rights reserved.
//

#import "AsyncImageLoader.h"
#import "ScheduleSectionViewController.h"
#import "UIConstants.h"

@interface ScheduleSectionViewController () {
    AsyncImageLoader *imageLoader;
}

@property (weak, nonatomic) IBOutlet UILabel *titleText;
@property (weak, nonatomic) IBOutlet UIImageView *weatherIconView;
@property (weak, nonatomic) IBOutlet UILabel *degreeText;

@end

@implementation ScheduleSectionViewController
@synthesize titleText = _titleText;
@synthesize weatherIconView = _weatherIconView;
@synthesize degreeText = _degreeText;

@synthesize weatherIcon = _weatherIcon;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        imageLoader = [[AsyncImageLoader alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark properties
- (void)setTitle:(NSString *)title {
    
    self.titleText.text = title;
}

- (NSString *)title {
    
    return self.titleText.text;
}

- (void)setWeatherIcon:(UIImage *)weatherIcon {
    self.weatherIconView.image = weatherIcon;
}

- (UIImage *)weatherIcon {
    
    return self.weatherIconView.image;
}

- (void)setTemperature:(NSInteger)temperature {
    
    if (temperature == noTemperature)
        self.degreeText.text = @"";
    else
        self.degreeText.text = [NSString stringWithFormat:@"%d C", temperature];
}

- (void)setImageURL:(NSString *)imageUrl {
    
    [imageLoader setLoadToImageView:self.weatherIconView];
    [imageLoader loadImageFromURL:[NSURL URLWithString:imageUrl]];
}

@end
