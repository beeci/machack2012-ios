//
//  ViewController.m
//  qEvents
//
//  Created by Bela Tibor Bartha on 12-05-04.
//  Copyright (c) 2012 Macadamian. All rights reserved.
//

#import "ScheduleViewController.h"
#import "UIConstants.h"
#import "NSDateFormatter+Helper.h"
#import "NSDate+Helper.h"
#import "AppDelegate.h"
#import "DetailsViewController.h"
#import "ScheduleSectionViewController.h"
#import "EventfulManager.h"
#import "EventfulDataDelegate.h"
#import "EventData.h"
#import "StorageManager.h"
#import "EventInfo.h"
#import "WeatherDetail.h"
#import "NewEventViewController.h"
#import <Twitter/Twitter.h>

static NSString *tweetText = @"Created new event, check: ";
static NSString *tweetURL = @"http://eventful.com/events/%@";

@interface ScheduleViewController () <EventfulDataDelegate> {
    NSMutableArray *sections;
}

@property (strong, nonatomic) NSDate *date;
@property (strong, nonatomic) NSMutableDictionary * eventsByHour;
@property (weak, nonatomic) IBOutlet UITableView * table;
@property (weak, nonatomic) IBOutlet UIView *spinner;

- (IBAction)handleRightSwipe:(UIGestureRecognizer *)recognizer;
- (IBAction)handleLeftSwipe:(UIGestureRecognizer *)recognizer;

@end

@implementation ScheduleViewController
@synthesize eventsByHour = _eventsByHour;
@synthesize date = _date;
@synthesize table = _table;
@synthesize spinner = _spinner;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    NSAssert(NO, @"Use init with date");
    return nil;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil date:(NSDate *)datetoShow {
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.date = [datetoShow clearedTime];
        sections = [[NSMutableArray alloc] initWithCapacity:numHoursInDay];
        _venues = [[NSMutableDictionary alloc] init];
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        appDelegate.eventfulManager.delegate = self;

    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] 
                                   initWithTitle:@"City"
                                   style:UIBarButtonItemStyleBordered
                                   target:self
                                   action:@selector(cityPressed:)];
    self.navigationItem.rightBarButtonItem = doneButton;
    
    UIImageView *tempImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"qEvents.background2.jpeg"]];
    [tempImageView setFrame:self.table.frame]; 
    
    self.table.backgroundView = tempImageView;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (void)reloadTable {
    [self.table reloadData];
}

- (void) viewWillAppear:(BOOL)animated {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //[appDelegate.eventfulManager searchOnCity:appDelegate.city day:self.date];
    
    [appDelegate.eventfulManager searchVenueList:appDelegate.city];
}

#pragma mark properties

- (void)setDate:(NSDate *)date {
    
    _date = date;
    NSDateFormatter *formatter = [NSDateFormatter shortDateFormatter];
    NSString *stringFromDate = [formatter stringFromDate:self.date];
    
    self.title = stringFromDate;
}

#pragma mark UITableViewDatasource & Delegate

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (void) reloadSectionHeaders {
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    WeatherDay *currentWeatherDay = nil;
    
    for (WeatherDay *day in appDelegate.tenDayForecast) {
        if ([day.date compare:self.date] == NSOrderedSame) {
            currentWeatherDay = day;
            break;
        }
    }
    
    for (int i = 0; i < numHoursInDay; i++) {
        ScheduleSectionViewController *sectionViewController = [sections objectAtIndex:i];
        for (WeatherDetail *detail in currentWeatherDay.hourlyForecast) {
            if (detail.starthour == i) {
                [sectionViewController setTemperature:detail.temperature];
                [sectionViewController setImageURL:detail.iconUrl];
                [sectionViewController.view setNeedsDisplay];
                break;
            }
            else if (detail.starthour > i) {
                break;
            }
        }
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    BOOL justLoaded = NO;
    if (sections.count == 0)
        justLoaded = YES;
    for (int i = 0; i < numHoursInDay; i++) {
        ScheduleSectionViewController *sectionViewController = [[ScheduleSectionViewController alloc] initWithNibName:@"ScheduleSectionViewController" bundle:nil];
        if (sectionViewController.view) {
            sectionViewController.title = [NSString stringWithFormat:@"%02d:00 - %02d:00", i, i + 1];
            [sectionViewController setTemperature:noTemperature];
        }
        [sections addObject:sectionViewController];
    }
    
    if (justLoaded)
        [self reloadSectionHeaders];
    
    return numHoursInDay;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {

    ScheduleSectionViewController *controller = (ScheduleSectionViewController *)[sections objectAtIndex:section];
    return controller.view;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSArray * events = [self.eventsByHour objectForKey:[NSNumber numberWithInteger:section]];
    
    int elements = 0;
    
    if (events) 
        elements = [events count];

    return elements + 1; 
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ScheduleCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    cell.textLabel.text = @"";
    cell.detailTextLabel.text = @"<Add Event>";
    
    NSArray * events = [self.eventsByHour objectForKey:[NSNumber numberWithInteger:indexPath.section]];
    
    if (events != nil && events.count > indexPath.row){
        
        EventData * data = [events objectAtIndex:indexPath.row];
        
        cell.textLabel.text = data.title;
        cell.detailTextLabel.text = data.description;
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        cell.textLabel.textColor = [UIColor blackColor];
        
        for (EventInfo *storageInfo in appDelegate.storageManager.eventInfoList){
            
            if ([storageInfo.userId isEqualToString:[appDelegate userId]] && [storageInfo.eventId isEqualToString:data.eventId] && storageInfo.eventId.length != 0) {
                // event was created by the currently authenticated user
                cell.textLabel.textColor = [UIColor blueColor];
            }
        }
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{    
    NSArray * array = [self.eventsByHour objectForKey:[NSNumber numberWithInteger: indexPath.section]];
    
    if (array != nil && indexPath.row < array.count){
        //Row contain valid data. We need to check if we created that appointment. 
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        DetailsViewController *detailsViewController = [[DetailsViewController alloc]initWithNibName:@"DetailsViewController" bundle:[NSBundle mainBundle]];
        
        NSArray * events = [self.eventsByHour objectForKey:[NSNumber numberWithInteger:indexPath.section]];
        
        if (events != nil && events.count > indexPath.row) {
            
            EventData * data = [events objectAtIndex:indexPath.row];
            detailsViewController.eventName = data.title;
            detailsViewController.eventDetails = data.description;
            detailsViewController.eventID = data.eventId;
            NSDateFormatter *formatter = [NSDateFormatter shortDateFormatter];
            detailsViewController.startDate = [formatter stringFromDate: data.startDate];
        }
        [detailsViewController setTweetButtonEnabled:NO];
        
        EventData * data = [array objectAtIndex:indexPath.row];
        
        for (EventInfo *storageInfo in appDelegate.storageManager.eventInfoList){
            
            if ([storageInfo.userId isEqualToString:[appDelegate userId]] && [storageInfo.eventId isEqualToString:[data eventId]] && storageInfo.eventId.length != 0) { 
                // event was created by the currently authenticated user therefore we can tweet
                
                [detailsViewController setTweetButtonEnabled:YES];
            }
        }
        
        [[self navigationController] pushViewController:detailsViewController animated:YES];
    } else {
        // Adding new row

        NewEventViewController *newEventViewController = [[NewEventViewController alloc]initWithNibName:@"NewEventViewController" bundle:[NSBundle mainBundle] date:[self.date setHours:indexPath.section]];
        newEventViewController.venuesDictionary = _venues;
        [[self navigationController] pushViewController:newEventViewController animated:YES];
    }
    
}

#pragma mark helpers
- (BOOL)isValidDate:(NSDate *)date {
    
    NSDate *minDate = [[NSDate date] clearedTime];
    NSDate *maxDate = [[[NSDate date] dateByAddingTimeInterval:(10 * oneDay)] clearedTime];
    
    if ([date compare:minDate] == NSOrderedAscending)
        return NO;
    if ([date compare:maxDate] == NSOrderedDescending)
        return NO;
    return YES;
}

#pragma mark Gesture recognizer
- (IBAction)handleRightSwipe:(UIGestureRecognizer *)recognizer {
    
    NSDate *newDate = [self.date dateByAddingTimeInterval:-oneDay];
    
    if (![self isValidDate:newDate])
        return;
    
    ScheduleViewController *newController = [[ScheduleViewController alloc] initWithNibName:@"ScheduleViewController" bundle:nil date:newDate];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate switchScheduleControllerFrom:self to:newController withTransition:UIViewAnimationTransitionFlipFromLeft];
}

- (IBAction)handleLeftSwipe:(UIGestureRecognizer *)recognizer {
    
    NSDate *newDate = [self.date dateByAddingTimeInterval:oneDay];
    
    if (![self isValidDate:newDate])
        return;
    
    ScheduleViewController *newController = [[ScheduleViewController alloc] initWithNibName:@"ScheduleViewController" bundle:nil date:newDate];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate switchScheduleControllerFrom:self to:newController withTransition:UIViewAnimationTransitionFlipFromRight];
}

#pragma mark EventfulDataDelegate

- (void) eventfulDataReceived : (NSDictionary * ) data type : (EventfulDataType) type {
    
    switch (type) {
        case SearchResult: {
            NSArray *events = [EventData parseJSON:data];
            
            if (!self.eventsByHour)
                self.eventsByHour = [[NSMutableDictionary alloc] init];
            else {
                [self.eventsByHour removeAllObjects];
            }
            
            for (int i = 0; i < numHoursInDay; i++) {
                
                NSMutableArray * array = [NSMutableArray array];
                
                for (EventData * data in events) {
                    if (data.startDate.hour == i)
                        [array addObject:data];
                }
                
                if ([array count] != 0)
                    [self.eventsByHour setObject:array forKey:[NSNumber numberWithInt:i]];
            }
            
            [self.spinner setHidden:YES];
            self.table.userInteractionEnabled = YES;
            [[self table] reloadData];
            break;
        }
        case NewEventResult: {
            
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            // events in the storage now has assigned date now (is not used) 
            [appDelegate.storageManager addEvent:appDelegate.userId startDate:[NSDate date] eventId: [data objectForKey:@"id"]];
            
            TWTweetComposeViewController *tweetSheet = [[TWTweetComposeViewController alloc] init];
            
            [tweetSheet setInitialText:tweetText];
            [tweetSheet addURL:[NSURL URLWithString:[NSString stringWithFormat:tweetURL, [data objectForKey:@"id"]]]];
            tweetSheet.completionHandler = ^(TWTweetComposeViewControllerResult result){
                    [self dismissModalViewControllerAnimated:YES];
                };
            
            [self presentModalViewController:tweetSheet animated:YES];
            break;
        }
        case VenueListResult: {
            
            // venue list arrives we can request search 
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            [appDelegate.eventfulManager searchOnCity:appDelegate.city day:self.date];
            
            [_venues removeAllObjects];
            
            NSDictionary * venues = [data objectForKey:@"venues"];
            
            if (![venues isKindOfClass:[NSNull class]]) {
                NSArray * venueList = [venues objectForKey:@"venue"];
            
                for (NSDictionary * keys in venueList) {
                    NSString * name = [keys objectForKey:@"name"];
                    NSString * venueId = [keys objectForKey:@"id"];
                    
                    [_venues setObject:name forKey:venueId];
                }
            }
            
            NSLog(@"%d venues received", [_venues count]);

            break;
        }
            
        default:
            break;
    }
}

#pragma mark city selector
- (IBAction)cityPressed:(id)sender {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate showCitySelector];
}


@end
