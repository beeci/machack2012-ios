//
//  StorageManager.h
//  qEvents
//
//  Created by Zoli on 5/4/12.
//  Copyright (c) 2012 Macadamian. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    UserInfoType,
    EventInfoType,
} RequestedItemType;

@interface StorageManager : NSObject {
    RequestedItemType _currentlyRequested;
}

@property (nonatomic) BOOL storageAvailable;
@property (strong) NSMetadataQuery *query;
@property (strong) NSMutableArray *userInfoList;
@property (strong) NSMutableArray *eventInfoList;
@property (strong, nonatomic) NSURL * ubiq;

- (void) initializeStorage;
- (void) loadUserInfoList;
- (void) loadEventInfoList;
- (void) addUser : (NSString*) userName password: (NSString *) password;
- (void) addEvent : (NSString*) userId startDate : (NSDate *) startDate eventId : (NSString *) eventId;

@end
