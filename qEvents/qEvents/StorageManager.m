//
//  StorageManager.m
//  qEvents
//
//  Created by Zoli on 5/4/12.
//  Copyright (c) 2012 Macadamian. All rights reserved.
//

#import "StorageManager.h"
#import "UserInfo.h"
#import "EventInfo.h"
#import "UIConstants.h"

@implementation StorageManager

@synthesize storageAvailable = _storageAvailable;
@synthesize query = _query;
@synthesize userInfoList = _userInfoList;
@synthesize eventInfoList = _eventInfoList;
@synthesize ubiq = _ubiq;

- (void) initializeStorage {
    
    self.storageAvailable = NO;
    self.userInfoList = [[NSMutableArray alloc] init];
    self.eventInfoList = [[NSMutableArray alloc] init];
    
    self.ubiq = [[NSFileManager defaultManager] URLForUbiquityContainerIdentifier:nil];
    
    if (self.ubiq) {
        NSLog(@"iCloud access at %@", self.ubiq);
        self.storageAvailable = YES;
        
    } else {
        NSLog(@"No iCloud access");
    }
}

- (void) loadEventInfoList {
    
    if (self.ubiq) {
        
        _currentlyRequested = EventInfoType;
        
        self.query = [[NSMetadataQuery alloc] init];
        [self.query setSearchScopes: [NSArray arrayWithObject: NSMetadataQueryUbiquitousDocumentsScope]];
        NSPredicate *pred = [NSPredicate predicateWithFormat: @"%K like 'EventInfo_*'", NSMetadataItemFSNameKey];
        [self.query setPredicate:pred];
        [[NSNotificationCenter defaultCenter] addObserver:self 
                                                 selector:@selector(queryDidFinishGathering:) 
                                                     name:NSMetadataQueryDidFinishGatheringNotification 
                                                   object:self.query];
        
        [self.query startQuery];
        
    } else {
        NSLog(@"No iCloud access");
    }
}

- (void) loadUserInfoList {
    
    if (self.ubiq) {
        
        _currentlyRequested = UserInfoType;
        
        self.query = [[NSMetadataQuery alloc] init];
        [self.query setSearchScopes: [NSArray arrayWithObject: NSMetadataQueryUbiquitousDocumentsScope]];
        NSPredicate *pred = [NSPredicate predicateWithFormat: @"%K like 'UserInfo_*'", NSMetadataItemFSNameKey];
        [self.query setPredicate:pred];
        [[NSNotificationCenter defaultCenter] addObserver:self 
                                                 selector:@selector(queryDidFinishGathering:) 
                                                     name:NSMetadataQueryDidFinishGatheringNotification 
                                                   object:self.query];
        
        [self.query startQuery];
        
    } else {
        NSLog(@"No iCloud access");
    }
}

- (void) addUser : (NSString*) userName password: (NSString *) password {
    
    if (self.ubiq) {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyyMMdd_hhmmss"];
        
        NSString *fileName = [NSString stringWithFormat:@"UserInfo_%@", [formatter stringFromDate:[NSDate date]]];
        
        NSURL *ubiquitousPackage = [[self.ubiq URLByAppendingPathComponent:@"Documents"] URLByAppendingPathComponent:fileName];
        
        UserInfo *doc = [[UserInfo alloc] initWithFileURL:ubiquitousPackage];
        doc.userName = userName;
        doc.password = password;
        doc.userId = [self stringWithUUID];
        
        [doc saveToURL:[doc fileURL] forSaveOperation:UIDocumentSaveForCreating completionHandler:^(BOOL success) {
            
            if (success) {
                
                [self.userInfoList addObject:doc];
            }
            
        }];
    } else {
        NSLog(@"No iCloud access");
    }
}

- (void) addEvent : (NSString*) userId startDate : (NSDate *) startDate eventId : (NSString *) eventId {
    
    if (self.ubiq) {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyyMMdd_hhmmss"];
        
        NSString *fileName = [NSString stringWithFormat:@"EventInfo_%@", [formatter stringFromDate:[NSDate date]]];
        
        NSURL *ubiquitousPackage = [[self.ubiq URLByAppendingPathComponent:@"Documents"] URLByAppendingPathComponent:fileName];
        
        EventInfo *doc = [[EventInfo alloc] initWithFileURL:ubiquitousPackage];
        doc.userId = userId;
        doc.startDate = startDate;
        doc.eventId = eventId;
        [doc saveToURL:[doc fileURL] forSaveOperation:UIDocumentSaveForCreating completionHandler:^(BOOL success) {
            
            if (success) {
                
                [self.eventInfoList addObject:doc];
            }
            
        }];
    } else {
        NSLog(@"No iCloud access");
    }
}

#pragma mark - Helper Methods 

- (NSString*) stringWithUUID {
    CFUUIDRef uuidObj = CFUUIDCreate(nil);
    NSString *uuidString = (__bridge NSString*)CFUUIDCreateString(nil, uuidObj);
    CFRelease(uuidObj);
    NSLog(@"UUID: %@ created.", uuidString);
    return uuidString;
}

- (void)queryDidFinishGathering:(NSNotification *)notification {
    
    NSMetadataQuery *query = [notification object];
    [query disableUpdates];
    [query stopQuery];
    
    [self loadData:query];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:NSMetadataQueryDidFinishGatheringNotification
                                                  object:query];
    
    self.query = nil;
    
}

- (void)loadData:(NSMetadataQuery *)query {
    
    if (_currentlyRequested == UserInfoType)
        [self.userInfoList removeAllObjects];
    if (_currentlyRequested == EventInfoType)
        [self.eventInfoList removeAllObjects];
    
    NSLog(@"%d items loaded from storage", [[query results] count]);
    
    for (NSMetadataItem *item in [query results]) {
        
        NSURL *url = [item valueForAttribute:NSMetadataItemURLKey];
        
        id doc;
        
        if (_currentlyRequested == UserInfoType)
            doc = [[UserInfo alloc] initWithFileURL:url];
        if (_currentlyRequested == EventInfoType)
            doc = [[EventInfo alloc] initWithFileURL:url];
        
        [doc openWithCompletionHandler:^(BOOL success) {
            if (success) {
                
                if (_currentlyRequested == UserInfoType) {
                    [self.userInfoList addObject:doc];
                    [[NSNotificationCenter defaultCenter] postNotificationName:kUserInfoListArrived object:self.userInfoList];
                }
                
                if (_currentlyRequested == EventInfoType)
                    [self.eventInfoList addObject:doc];
                
                // sending notification when the list was received
                
            } else {
                NSLog(@"failed to open from iCloud");
            }
            
        }];        
    }    
    

}
@end
