//
//  UIConstants.h
//  qEvents
//
//  Created by Bela Tibor Bartha on 12-05-04.
//  Copyright (c) 2012 Macadamian. All rights reserved.
//

#ifndef qEvents_UIConstants_h
#define qEvents_UIConstants_h

static const int numHoursInDay = 24;
static const int oneDay = 60 * 60 * 24;

static const int searchBarHeight = 45;
static const int noTemperature = -256;

#define kSearchResultArrived @"SearchResultArrived"
#define kUserInfoListArrived @"UserInfoListArrived"

#endif
