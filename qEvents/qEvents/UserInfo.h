//
//  UserInfo.h
//  qEvents
//
//  Created by Zoli on 5/4/12.
//  Copyright (c) 2012 Macadamian. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    userName,
    password,
    userId,
} fieldsTypeUserInfo;

@interface UserInfo : UIDocument

@property (strong, nonatomic) NSString* userName;
@property (strong, nonatomic) NSString* password;
@property (strong, nonatomic) NSString* userId;

@end
