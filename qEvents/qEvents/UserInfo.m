//
//  UserInfo.m
//  qEvents
//
//  Created by Zoli on 5/4/12.
//  Copyright (c) 2012 Macadamian. All rights reserved.
//

#import "UserInfo.h"

@implementation UserInfo

@synthesize userId = _userId;
@synthesize userName = _userName;
@synthesize password = _password;

static NSString *userNameKey = @"userNameKey";
static NSString *passwordKey = @"passwordKey";
static NSString *userIdKey = @"userId";

- (NSData *) serialize {
    
    NSString * serializedData = [NSString stringWithFormat:@"%@|%@|%@", self.userName, self.password, self.userId];
    
    return [NSData dataWithBytes:[serializedData UTF8String] length:[serializedData length]];;
}

- (void) deserialize : (NSData *) data {
    
    NSString * stringData = [[NSString alloc]  initWithBytes:[data bytes] length:[data length] encoding:NSUTF8StringEncoding];
    
    NSArray * fields = [stringData componentsSeparatedByString:@"|"];
    
    for (int i = 0; i < fields.count; i++) {
        
        NSString * field = [fields objectAtIndex:i];
        
        switch (i) {
            case userName:
                self.userName = field;
                break;
            case password:
                self.password = field;
                break;
            case userId:
                self.userId = field;
                break;
            default:
                break;
        }
    }
    
}

// Called whenever the application reads data from the file system
- (BOOL)loadFromContents:(id)contents ofType:(NSString *)typeName error:(NSError **)outError{

    if ([contents length] > 0) {    
        [self deserialize:contents];
    }
    
    return YES;    
}

// Called whenever the application (auto)saves the content of a note
- (id)contentsForType:(NSString *)typeName error:(NSError **)outError 
{
    return [self serialize];
    
}

@end
