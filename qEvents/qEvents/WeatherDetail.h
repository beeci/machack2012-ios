//
//  WeatherDetail.h
//  qEvents
//
//  Created by Bela Tibor Bartha on 12-05-05.
//  Copyright (c) 2012 Macadamian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WeatherDay : NSObject

@property (strong, nonatomic) NSDate *date;
@property (strong, nonatomic) NSMutableArray *hourlyForecast;

@end

@interface WeatherDetail : NSObject

@property (assign, nonatomic) NSInteger starthour;
@property (strong, nonatomic) NSString *iconUrl;
@property (assign, nonatomic) NSInteger temperature;

@end
