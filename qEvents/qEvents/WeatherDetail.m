//
//  WeatherDetail.m
//  qEvents
//
//  Created by Bela Tibor Bartha on 12-05-05.
//  Copyright (c) 2012 Macadamian. All rights reserved.
//

#import "WeatherDetail.h"

@implementation WeatherDay
@synthesize date = _date;
@synthesize hourlyForecast = _hourlyForecast;
@end

@implementation WeatherDetail
@synthesize starthour = _starthour;
@synthesize iconUrl = _iconUrl;
@synthesize temperature = _temperature;

@end
