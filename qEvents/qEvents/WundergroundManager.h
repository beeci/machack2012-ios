//
//  WundergroundManager.h
//  qEvents
//
//  Created by Maria on 5/5/12.
//  Copyright (c) 2012 Macadamian. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SBJsonStreamParser;
@class SBJsonStreamParserAdapter;

@protocol WundergroundManagerDelegate <NSObject>

- (void)resultArrived;

@end

@interface WundergroundManager : NSObject {
    SBJsonStreamParser *parser;
    NSURLConnection *theConnection;
    SBJsonStreamParserAdapter *adapter;
}

- (void)searchOnCity :(NSString *)city;

@property (weak, nonatomic) id<WundergroundManagerDelegate> delegate;
@property (strong, nonatomic) NSArray *tenDayForecast;
@end
