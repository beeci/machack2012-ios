//
//  WundergroundManager.m
//  qEvents
//
//  Created by Maria on 5/5/12.
//  Copyright (c) 2012 Macadamian. All rights reserved.
//

#import "WundergroundManager.h"
#import "SBJson.h"
#import "UIConstants.h"
#import "WeatherDetail.h"
#import "NSDate+Helper.h"

static NSString *wunderground_app_key = @"a1e10e09cb94ae52"; // application key set on wunderground
static NSString *wunderground_url = @"http://api.wunderground.com/api/%@/hourly10day/q/%@.json";

@interface WundergroundManager () <SBJsonStreamParserAdapterDelegate>


@end

@implementation WundergroundManager
@synthesize delegate = _delegate;
@synthesize tenDayForecast = _tenDayForecast;

- (void)searchOnCity :(NSString *)city {
    // We don't want *all* the individual messages from the
	// SBJsonStreamParser, just the top-level objects. The stream
	// parser adapter exists for this purpose.
	adapter = [[SBJsonStreamParserAdapter alloc] init];
	
	// Set ourselves as the delegate, so we receive the messages
	// from the adapter.
	adapter.delegate = self;
	
	// Create a new stream parser..
	parser = [[SBJsonStreamParser alloc] init];
	
	// .. and set our adapter as its delegate.
	parser.delegate = adapter;
	
	// Normally it's an error if JSON is followed by anything but
	// whitespace. Setting this means that the parser will be
	// expecting the stream to contain multiple whitespace-separated
	// JSON documents.
	parser.supportMultipleDocuments = YES;
    
    NSString *url = [NSString stringWithFormat:wunderground_url, wunderground_app_key, [city stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
	NSURLRequest *theRequest=[NSURLRequest requestWithURL:[NSURL URLWithString:url]
											  cachePolicy:NSURLRequestUseProtocolCachePolicy
										  timeoutInterval:60.0];
	
	theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
}

#pragma mark SBJsonStreamParserAdapterDelegate methods

- (void)parser:(SBJsonStreamParser *)parser foundArray:(NSArray *)array {
    [NSException raise:@"unexpected" format:@"Should not get here"];
}

- (void)parser:(SBJsonStreamParser *)parser foundObject:(NSDictionary *)dict {
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:[NSString stringWithFormat:@"%@", kSearchResultArrived]
                                                  object:dict];
    
    NSDictionary *forecast = [dict objectForKey:@"hourly_forecast"];
    
    NSMutableArray *list = [[NSMutableArray alloc] init];
    
    NSDate *currentdate = nil;
    WeatherDay *currentWeatherDay = nil;

    for (NSDictionary *dayForecast in forecast) {
        NSDictionary *time = [dayForecast objectForKey:@"FCTTIME"];
        NSInteger year = [(NSString *)[time objectForKey:@"year"] intValue];
        NSInteger month = [(NSString *)[time objectForKey:@"mon"] intValue];
        NSInteger day = [(NSString *)[time objectForKey:@"mday"] intValue];
        NSInteger hour = [(NSString *)[time objectForKey:@"hour"] intValue];
        
        NSDate *date = [NSDate dateFromYear:year month:month day:day];
        
        WeatherDetail *detail = [[WeatherDetail alloc] init];
        detail.starthour = hour;
        detail.iconUrl = [dayForecast objectForKey:@"icon_url"];
        detail.temperature = [(NSString *)[[dayForecast objectForKey:@"temp"] objectForKey:@"metric"] intValue];
        
        if ([date compare:currentdate] != NSOrderedSame || currentdate == nil) {
            currentWeatherDay = [[WeatherDay alloc] init];
            currentWeatherDay.date = date;
            currentWeatherDay.hourlyForecast = [[NSMutableArray alloc] init];
            [list addObject:currentWeatherDay];
            currentdate = date;
        }
        [currentWeatherDay.hourlyForecast addObject:detail];
    }
    
    _tenDayForecast = [NSArray arrayWithArray:list];
    
    [_delegate resultArrived];
}

#pragma mark NSURLConnectionDelegate methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	NSLog(@"Connection didReceiveResponse: %@ - %@", response, [response MIMEType]);
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
	NSLog(@"Connection didReceiveAuthenticationChallenge: %@", challenge);
	
	/*NSURLCredential *credential = [NSURLCredential credentialWithUser:username.text
     password:password.text
     persistence:NSURLCredentialPersistenceForSession];
     
     [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];*/
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	NSLog(@"Connection didReceiveData of length: %u", data.length);
    
    NSString * result = [[NSString alloc]  initWithBytes:[data bytes] length:[data length] encoding:NSUTF8StringEncoding];
    
    NSLog(@"%@", result);
	
	// Parse the new chunk of data. The parser will append it to
	// its internal buffer, then parse from where it left off in
	// the last chunk.
	SBJsonStreamParserStatus status = [parser parse:data];
	
	if (status == SBJsonStreamParserError) {
		NSLog(@"Parser error: %@", parser.error);
		
	} else if (status == SBJsonStreamParserWaitingForData) {
		NSLog(@"Parser waiting for more data");
	}
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"Connection failed! Error - %@ %@",
          [error localizedDescription],
          [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
}

@end
